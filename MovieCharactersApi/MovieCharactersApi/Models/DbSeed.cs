﻿using MovieCharacters.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Models
{
    public static class DbSeed
    {
        /// <summary>
        /// Used to populate the database with movies
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Movie> SeedMovies()
        {
            List<Movie> movies = new List<Movie>();

            movies.Add(new Movie()
            {
                Id = 1,
                Title = "Fast & Furious",
                Genre = "Action",
                ReleaseYear = "2001",
                Director = "Rob Cohen",
                PictureUrl = "https://upload.wikimedia.org/wikipedia/en/5/54/Fast_and_the_furious_poster.jpg",
                Trailer = "https://www.youtube.com/watch?v=2TAOizOnNPo",
                FranchiseId = 1

            });
            movies.Add(new Movie()
            {
                Id = 2,
                Title = "2 Fast 2 Furious",
                Genre = "Action",
                ReleaseYear = "2003",
                Director = "John Singleton",
                PictureUrl = "https://upload.wikimedia.org/wikipedia/en/9/9d/Two_fast_two_furious_ver5.jpg",
                Trailer = "https://www.youtube.com/watch?v=F_VIM03DXWI",
                FranchiseId = null

            }) ;
            movies.Add(new Movie()
            {
                Id = 3,
                Title = "The Fast and the Furious: Tokyo Drift",
                Genre = "Action",
                ReleaseYear = "2006",
                Director = "Justin Lin",
                PictureUrl = "https://upload.wikimedia.org/wikipedia/en/4/4f/Poster_-_Fast_and_Furious_Tokyo_Drift.jpg",
                Trailer = "https://www.youtube.com/watch?v=p8HQ2JLlc4E",
                FranchiseId = 1

            });
            movies.Add(new Movie()
            {
                Id = 4,
                Title = "Batman Begins",
                Genre = "Action",
                ReleaseYear = "2005",
                Director = "Christopher Nolen",
                PictureUrl = "https://m.media-amazon.com/images/M/MV5BOTY4YjI2N2MtYmFlMC00ZjcyLTg3YjEtMDQyM2ZjYzQ5YWFkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg",
                Trailer = "https://www.youtube.com/watch?v=neY2xVmOfUM",
                FranchiseId = 2

            });
            movies.Add(new Movie()
            {
                Id = 5,
                Title = "The Dark night",
                Genre = "Action",
                ReleaseYear = "2008",
                Director = "Christopher Nolen",
                PictureUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOH1Z3WEv-NS_ahc7NSQRz1LTh813Z6sMmI_E7d1_FYpQd0MxY",
                Trailer = "https://www.youtube.com/watch?v=EXeTwQWrcwY",
                FranchiseId = 2

            });
            movies.Add(new Movie()
            {
                Id = 6,
                Title = "The Dark Knight Rises",
                Genre = "Action",
                ReleaseYear = "2012",
                Director = "Christopher Nolen",
                PictureUrl = "https://m.media-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_FMjpg_UX1000_.jpg",
                Trailer = "https://www.youtube.com/watch?v=g8evyE9TuYk",
                FranchiseId = 2

            });
            movies.Add(new Movie()
            {
                Id = 7,
                Title = "The Lord of the Rings: The Fellowship of the Ring",
                Genre = "Fantasy/Adventure",
                ReleaseYear = "2001",
                Director = "Peter Jackson",
                PictureUrl = "https://d2iltjk184xms5.cloudfront.net/uploads/photo/file/127849/small_original.jpeg",
                Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                FranchiseId = 3

            });
            movies.Add(new Movie()
            {
                Id = 8,
                Title = "The Lord of the Rings: The Two Towers",
                Genre = "Fantasy/Adventure",
                ReleaseYear = "2002",
                Director = "Peter Jackson",
                PictureUrl = "https://cdn.cdon.com/media-dynamic/images/product/movie/dvd/image2/lord_of_the_rings_-_the_two_towers_theatrical_cut_nordic-43043783-.jpg?impolicy=product&w=1280&h=720",
                Trailer = "https://www.youtube.com/watch?v=LbfMDwc4azU",
                FranchiseId = 3

            });
            movies.Add(new Movie()
            {
                Id = 9,
                Title = "The Lord of the Rings: The Return of the King",
                Genre = "Fantasy/Adventure",
                ReleaseYear = "2003",
                Director = "Peter Jackson",
                PictureUrl = "https://upload.wikimedia.org/wikipedia/en/b/be/The_Lord_of_the_Rings_-_The_Return_of_the_King_%282003%29.jpg",
                Trailer = "https://www.youtube.com/watch?v=y2rYRu8UW8M",
                FranchiseId = 3

            });
            return movies;

        }


        /// <summary>
        /// Used to seed the populate with franchises
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Franchise> SeedFranchise()
        {
            List<Franchise> franchises = new List<Franchise>();

            franchises.Add(new Franchise()
            {
                Id = 1,
                Name = "The Fast and Furious",
                Description = "Fast & Furious (also known as The Fast and the Furious)" +
                " is a media franchise centered on a series of action films that are " +
                "largely concerned with illegal street racing, heists, and spies. " +
                "The franchise also includes short films, a television series, " +
                "live shows, video games, and theme park attractions. " +
                "It is distributed by Universal Pictures.",

            });
            franchises.Add(new Franchise()
            {
                Id = 2,
                Name = "Batman series",
                Description = "Batman Begins is a reboot of the entire Batman film franchise under the direction of Christopher Nolan. " +
                "It revolves around Bruce Wayne journey into becoming The Dark Knight of Gotham City from crime and corruption . " +
                "In his quest he encounters allies that aid him in his quest such as his loyal butler and confident Alfred, local police officer and friend Jim Gordon, fellow gadgetry and tech equipment specialist Lucius Fox and finally childhood friend Rachel Dawes. " +
                "With the rises of Batman came enemies to challenge him such as Joker and the Scarecrow.",

            });
            franchises.Add(new Franchise()
            {
                Id = 3,
                Name = "The lord of the rings",
                Description = "The Lord of the Rings is the saga of a group of sometimes reluctant " +
                   "heroes who set forth to save their world from consummate evil. Its many worlds and " +
                   "creatures were drawn from Tolkien’s extensive knowledge of philology and folklore.",

            });

            return franchises;
        }


        /// <summary>
        /// Used to seed the populate with characters
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Character> SeedCharacters()
        {
            List<Character> characters = new List<Character>();

            characters.Add(new Character()
            {
                 Id = 1,
                 FullName = "Dominic Toretto",
                 Alias = "",
                 Gender = "Male",
                 Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Vin_Diesel_XXX_Return_of_Xander_Cage_premiere.png/800px-Vin_Diesel_XXX_Return_of_Xander_Cage_premiere.png"
            });
            characters.Add(new Character()
            {
                Id = 2,
                FullName = "Brian O'Connor",
                Alias = "",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/commons/9/91/PaulWalkerEdit-1.jpg"
            });
            characters.Add(new Character()
            {
                Id = 3,
                FullName = "Gandalf",
                Alias = "Gandalf",
                Gender = "Male",
                Picture = "https://static.feber.se/article_images/31/44/88/314488_980.gif"
            });
            characters.Add(new Character()
            {
                Id = 4,
                FullName = "Frodo Bagger",
                Alias = "",
                Gender = "Male",
                Picture = "https://i.pinimg.com/originals/63/77/c6/6377c6c26401f23739439440d67957f4.jpg"
            });
            characters.Add(new Character()
            {
                Id = 5,
                FullName = "Sam Gamgi",
                Alias = "",
                Gender = "Male",
                Picture = "https://i.pinimg.com/736x/ec/a6/2a/eca62aea2ab4b143ed58e5285b57dfa9--samwise-gamgee-hair-pulled-back.jpg"
            });
            characters.Add(new Character()
            {
                Id = 6,
                FullName = "Gollum",
                Alias = "Sméagol",
                Gender = "Male",
                Picture = "https://imgs.aftonbladet-cdn.se/v2/images/72739752-fe8b-4ec1-95e0-215cf8accbc3?fit=crop&h=1132&q=50&w=800&s=bd3e449b3b3e7431845ddd9f2c3d640499c21678"
            });
            characters.Add(new Character()
            {
                Id = 7,
                FullName = "Bruce Wayne",
                Alias = "Batman",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/1/19/Bruce_Wayne_%28The_Dark_Knight_Trilogy%29.jpg"
            });
            characters.Add(new Character()
            {
                Id = 8,
                FullName = "Joker",
                Alias = "Red hood",
                Gender = "Male",
                Picture = "https://static.wikia.nocookie.net/batman/images/f/f9/Heath_Ledger_as_the_Joker.JPG/revision/latest/top-crop/width/360/height/450?cb=20090903145508"
            });
            characters.Add(new Character()
            {
                Id = 9,
                FullName = "Unknown",
                Alias = "Bane",
                Gender = "Male",
                Picture = "https://static.posters.cz/image/1300/plakater/batman-dark-knight-rises-bane-sewer-i12721.jpg"
            });
            return characters;
        }

    }
}
