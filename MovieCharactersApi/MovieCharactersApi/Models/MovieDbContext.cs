﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieCharacters.Model
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        /// <summary>
        /// Needed for startup to work
        /// </summary>
        /// <param name="options"></param>
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }
        /// <summary>
        /// Populates the DbSets with Franchises, Movies and Characters from DbSeed. 
        /// It also configures some many to many relationships using the modelbuilder on "CharacterMovie" table.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Franchise>().HasData(DbSeed.SeedFranchise());

            modelBuilder.Entity<Character>().HasData(DbSeed.SeedCharacters());

            modelBuilder.Entity<Movie>().HasData(DbSeed.SeedMovies());

            // Seed M2M 
            modelBuilder.Entity<Character>()
            .HasMany(c => c.Movies)
            .WithMany(m => m.Characters)
            .UsingEntity<Dictionary<string, object>>(
            "CharacterMovie",
            r => r.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
            l => l.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
            je =>
            {
                je.HasKey("CharactersId", "MoviesId");
                je.HasData(
                //The fast and furious
                new { CharactersId = 1, MoviesId = 1 },
                new { CharactersId = 2, MoviesId = 1 },
                new { CharactersId = 1, MoviesId = 2 },
                new { CharactersId = 2, MoviesId = 2 },
                new { CharactersId = 1, MoviesId = 3 },
                new { CharactersId = 2, MoviesId = 3 },

                //Batman 
                new { CharactersId = 7, MoviesId = 4 },
                new { CharactersId = 7, MoviesId = 5 },
                new { CharactersId = 7, MoviesId = 6 },
                new { CharactersId = 8, MoviesId = 5 },
                new { CharactersId = 9, MoviesId = 6 },

                //The lord of the rings
                new { CharactersId = 3, MoviesId = 7 },
                new { CharactersId = 4, MoviesId = 7 },
                new { CharactersId = 5, MoviesId = 7 },
                new { CharactersId = 6, MoviesId = 7 },
                new { CharactersId = 3, MoviesId = 8 },
                new { CharactersId = 4, MoviesId = 8 },
                new { CharactersId = 5, MoviesId = 8 },
                new { CharactersId = 6, MoviesId = 8 },
                new { CharactersId = 3, MoviesId = 9 },
                new { CharactersId = 4, MoviesId = 9 },
                new { CharactersId = 5, MoviesId = 9 },
                new { CharactersId = 6, MoviesId = 9 }
                );
            });
        }
    }
}
