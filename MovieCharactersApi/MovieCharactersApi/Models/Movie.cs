﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieCharacters.Model
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [MaxLength(4)]
        public string ReleaseYear { get; set; }
        [MaxLength(100)]
        public string Director { get; set; }
        public string PictureUrl { get; set; }
        public string Trailer { get; set; }

        //optional Foreign Key
        public int? FranchiseId { get; set; }

        //Navigational properties
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }

    }
}
