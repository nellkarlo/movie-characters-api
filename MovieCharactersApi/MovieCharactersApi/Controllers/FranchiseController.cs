﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacters.Model;
using MovieCharactersApi.Models.DTOs.CharacterDTO;
using MovieCharactersApi.Models.DTOs.FranchiseDTO;
using MovieCharactersApi.Models.DTOs.MovieDTO;
using MovieCharactersApi.Service.IService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Swashbuckle;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.EntityFrameworkCore;

namespace MovieCharactersApi.Controllers
{
    [ApiController]
    [Route("franchises")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchiseController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMovieService _movieService;

        private readonly IMapper _mapper;
        public FranchiseController(IFranchiseService franchiseService, IMovieService movieService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _movieService = movieService;
            _mapper = mapper;
        }


        /// <summary>
        /// Retrieve all franchises from database.
        /// </summary>
        /// <remarks>Returns empty list if no franchises exist</remarks>
        /// <returns>List of Franchises</returns>
        [SwaggerResponse(200, "Retrieved all franchises")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return Ok(_mapper.Map<IEnumerable<FranchiseReadDTO>>(await _franchiseService.GetAllAsync()));
        }

        /// <summary>
        /// Add a franchise to database.
        /// </summary>
        /// <remarks>
        /// - Name max length is 200 characters.
        /// </remarks>
        /// <param name="franchise">Franchise to add</param>
        /// <returns>The franchise added</returns>
        /// <exception cref="DBConcurrencyException">Could not add franchise to database. Returns 409 Conflict</exception>
        /// <exception cref="DbUpdateException">Invalid string length. Check string length requirements! Returns 400 Bad Request</exception>
        [SwaggerResponse(201, "Franchise created")]
        [SwaggerResponse(400, "Invalid string length. Check string length requirements!")]
        [SwaggerResponse(409, "Could not add franchise to database!")]
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise([FromBody] FranchiseCreateDTO franchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchise);
            try
            {
                domainFranchise = await _franchiseService.AddAsync(domainFranchise);

                return CreatedAtAction("GetFranchise",
                    new { id = domainFranchise.Id },
                    _mapper.Map<FranchiseReadDTO>(domainFranchise));
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Get a franchise from database.
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>The franchise</returns>
        [SwaggerResponse(200, "Franchise found")]
        [SwaggerResponse(404, "Could not find a franchise matching the id provided")]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetSpecificAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            
            return Ok(_mapper.Map<FranchiseReadDTO>(franchise));
        }


        /// <summary>
        /// Update a franchise
        /// </summary>
        /// <remarks>
        /// - Name max length is 200 characters.
        /// </remarks>
        /// <param name="id">Franchise id</param>
        /// <param name="franchise">Franchise with fields updated</param>
        /// <returns></returns>
        /// <exception cref="DBConcurrencyException">Could not update the franchise. Returns 409 Conflict</exception>
        /// <exception cref="DbUpdateException">Invalid string length. Check string length requirements! Returns 400 Bad Request</exception>
        [SwaggerResponse(204, "The franchise was updated")]
        [SwaggerResponse(400, "Invalid string length. Check string length requirements!")]
        [SwaggerResponse(404, "The franchise does not exist")]
        [SwaggerResponse(409, "Could not update the franchise!")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, [FromBody] FranchiseEditDTO franchise)
        {
            if (!_franchiseService.Exists(id)) return NotFound();

            var domainFranchise = _mapper.Map<Franchise>(franchise);

            domainFranchise.Id = id;

            try
            {
                await _franchiseService.UpdateAsync(domainFranchise);
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
            return NoContent();
        }


        /// <summary>
        /// Remove a franchise
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns></returns>
        /// <exception cref="DBConcurrencyException">Could not delete the franchise. Returns 409 Conflict</exception>
        [SwaggerResponse(204, "The franchise was removed")]
        [SwaggerResponse(404, "The franchise could not be found")]
        [SwaggerResponse(409, "Could not delete the franchise!")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.Exists(id)) return NotFound();
            
            try
            {
                await _franchiseService.DeleteAsync(id);
            }
            catch (DBConcurrencyException) 
            {
                return Conflict();
            }
            return NoContent();
        }


        /// <summary>
        /// Get the characters in a franchise
        /// </summary>
        /// <remarks>Returns empty list if no characters found</remarks>
        /// <param name="id">Franchise Id</param>
        /// <returns>List of Characters</returns>
        [SwaggerResponse(200, "Retrieved characters if there were any")]
        [SwaggerResponse(404, "Franchise does not exist")]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            if (!_franchiseService.Exists(id)) return NotFound();

            var franchiseCharacters = await _franchiseService.GetCharactersInFranchise(id);

            return Ok(_mapper.Map<IEnumerable<CharacterReadDTO>>(franchiseCharacters));
        }


        /// <summary>
        /// Gets the movies from the franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>List of Movies</returns>
        [SwaggerResponse(200, "Retrieved movies if there were any")]
        [SwaggerResponse(404, "Franchise does not exist")]
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            if (!_franchiseService.Exists(id)) return NotFound();

            var franchiseMovies = await _franchiseService.GetMoviesInFranchise(id);

            return Ok(_mapper.Map<IEnumerable<MovieReadDTO>>(franchiseMovies));
        }


        /// <summary>
        /// Add a movie to an existing franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <param name="movieid">Movie id</param>
        /// <returns>The Movie added with its franchise id updated</returns>
        /// <exception cref="DBConcurrencyException">Could not update the movie franchise id. Returns 409 Conflict</exception>
        [SwaggerResponse(404, "Franchise or movie does not exist")]
        [SwaggerResponse(201, "Movie has been updated with the new franchise id")]
        [SwaggerResponse(409, "Could not update the movie franchise id")]
        [HttpPost("{id}/movies/{movieid}")]
        public async Task<ActionResult<MovieReadDTO>> AddMovieToFranchise(int id, int movieid)
        {
            if (!_franchiseService.Exists(id) || !_movieService.Exists(movieid)) return NotFound();

            var domainMovie = await _movieService.GetSpecificAsync(movieid);
            domainMovie.FranchiseId = id;

            try
            {
                await _movieService.UpdateAsync(domainMovie);
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }
            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, _mapper.Map<MovieReadDTO>(domainMovie));
        }


        /// <summary>
        /// Removes movie from franchise
        /// </summary>
        /// <param name="movieid">Movie id</param>
        /// <returns>The movie without a franchise</returns>
        /// <exception cref="DBConcurrencyException">Could not remove the movies franchise id. Returns 409 Conflict</exception>
        [SwaggerResponse(404, "Franchise or movie does not exist")]
        [SwaggerResponse(201, "Franchise id has been removed from the movie")]
        [SwaggerResponse(409, "Could not remove franchise from movie!")]
        [HttpDelete("movies/{movieid}")]
        public async Task<ActionResult<MovieReadDTO>> RemoveMovieFromFranchise(int movieid)
        {
            if (!_movieService.Exists(movieid)) return NotFound();


            var domainMovie = await _movieService.GetSpecificAsync(movieid);
            domainMovie.FranchiseId = null;

            try
            {
                await _movieService.UpdateAsync(domainMovie);
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, _mapper.Map<MovieReadDTO>(domainMovie));
        }


        /// <summary>
        /// Get movie helper class
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns></returns>
        [SwaggerResponse(404, "Movie does not exist")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var Movie = await _movieService.GetSpecificAsync(id);
            if (Movie == null)
            {
                NotFound();
            }
            return _mapper.Map<MovieReadDTO>(Movie);
        }
    }
}
