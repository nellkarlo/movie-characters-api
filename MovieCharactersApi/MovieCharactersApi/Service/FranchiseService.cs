﻿using Microsoft.EntityFrameworkCore;
using MovieCharacters.Model;
using MovieCharactersApi.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Service
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Adds a franchise to database
        /// </summary>
        /// <param name="franchise">The franchise to add to database</param>
        /// <returns>The franchise added</returns>
        public async Task<Franchise> AddAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }
        /// <summary>
        /// Deletes franchise from database
        /// </summary>
        /// <param name="id">Id of the franchise to delete</param>
        /// <returns></returns>
        public async Task DeleteAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Remove(franchise);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Checks if the provided id matches any existing franchise in database
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>True if exists, otherwise false</returns>
        public bool Exists(int id)
        {
            return _context.Franchises.Any(c => c.Id == id);
        }
        /// <summary>
        /// Gets all the franchises in the database
        /// </summary>
        /// <returns>An Enumerable list of franchises</returns>
        public async Task<IEnumerable<Franchise>> GetAllAsync()
        {
            return await _context.Franchises.ToListAsync();
        }
        /// <summary>
        /// Retrieves the characters in a franchise matching the franchise id provided
        /// </summary>
        /// <param name="franchiseId">The id of the franchise</param>
        /// <returns>An Enumerable list of Characters</returns>
        public async Task<IEnumerable<Character>> GetCharactersInFranchise(int franchiseId)
        {
            var characterslist = await _context.Characters.Include(c=>c.Movies).ToListAsync();

            List<Character> selectedCharacters = new List<Character>();

            foreach (var character in characterslist)
            {
                foreach (var movie in character.Movies)
                {
                    if ((movie.FranchiseId == franchiseId))
                    {
                        if (selectedCharacters.Contains(character))
                        {
                            break;
                        }
                        else selectedCharacters.Add(character);
                    }
                }
            }
            return selectedCharacters;
        }
        /// <summary>
        /// Retrieves the movies in a franchise matching the franchise id provided
        /// </summary>
        /// <param name="franchiseId">The id of the franchise</param>
        /// <returns>An Enumerable list of movies</returns>
        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int franchiseId)
        {
            return await _context.Movies.Where(m => m.FranchiseId== franchiseId)
                .ToListAsync();
        }
        /// <summary>
        /// Retrieves a specific franchise matching the id provided
        /// </summary>
        /// <param name="id">The id of the franchise</param>
        /// <returns></returns>
        public async Task<Franchise> GetSpecificAsync(int id)
        {
            return await _context.Franchises.Where(c => c.Id == id).SingleOrDefaultAsync();
        }
        /// <summary>
        /// Uses entitystate to update the franchise in database. Then runs SaveChangesAsync
        /// </summary>
        /// <param name="franchise">The franchise to update</param>
        /// <returns></returns>
        public async Task UpdateAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
