﻿using MovieCharacters.Model;
using MovieCharactersApi.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MovieCharactersApi.Service
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;
        /// <summary>
        /// Sets context through Dependency Injection
        /// </summary>
        /// <param name="context">MovieDbContext</param>
        public MovieService(MovieDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Adds a movie to database
        /// </summary>
        /// <param name="movie">The movie to add to database</param>
        /// <returns>The movie added</returns>
        public async Task<Movie> AddAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Deletes movie from database
        /// </summary>
        /// <param name="id">Id of the movie to delete</param>
        /// <returns></returns>
        public async Task DeleteAsync(int id)
        {
            var Movie = await _context.Movies.FindAsync(id);
            _context.Remove(Movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks if the provided id matches any existing movie in database
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns>True if exists, otherwise false</returns>
        public bool Exists(int id)
        {
            return _context.Movies.Any(c => c.Id == id);
        }

        /// <summary>
        /// Uses entitystate to update the movie in database. Then runs SaveChangesAsync
        /// </summary>
        /// <param name="movie">The movie to update</param>
        /// <returns></returns>
        public async Task UpdateAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets all the movies in the database
        /// </summary>
        /// <returns>An Enumerable list of movies</returns>
        public async Task<IEnumerable<Movie>> GetAllAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        /// <summary>
        /// Retrieves a specific movie matching the id provided
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <returns></returns>
        public async Task<Movie> GetSpecificAsync(int id)
        {
            return await _context.Movies.Where(c => c.Id == id).Include(m=>m.Characters).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Retrieves the characters in a movie matching the movie id provided
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <returns>An Enumerable list of Characters in movie</returns>
        public async Task<IEnumerable<Character>> GetCharactersInMovie(int movieId)
        {
            var movie = await _context.Movies.Where(f => f.Id == movieId)
                .Include(f => f.Characters)
                .Select(f => f.Characters).ToListAsync();

            List<Character> CharacterList = new List<Character>();

            foreach (var characters in movie)
            {
                foreach (var character in characters)
                {
                    CharacterList.Add(character);
                }
            }
            return CharacterList.Distinct();

        }
    }
}
