﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Service.IService
{
    public interface IGenericCrudService<T>
    {
        public Task<IEnumerable<T>> GetAllAsync();
        public Task<T> GetSpecificAsync(int id);
        public Task<T> AddAsync(T t);
        public Task UpdateAsync(T t);
        public Task DeleteAsync(int id);
        public bool Exists(int id);
    }
}
