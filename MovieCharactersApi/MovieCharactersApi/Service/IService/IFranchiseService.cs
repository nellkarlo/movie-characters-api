﻿using MovieCharacters.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Service.IService
{
    public interface IFranchiseService : IGenericCrudService<Franchise>
    {
        Task<IEnumerable<Character>> GetCharactersInFranchise(int franchiseId);
        Task<IEnumerable<Movie>> GetMoviesInFranchise(int id);
    }
}
