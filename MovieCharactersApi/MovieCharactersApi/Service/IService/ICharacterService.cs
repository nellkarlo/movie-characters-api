﻿using MovieCharacters.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Service.IService
{
    public interface ICharacterService : IGenericCrudService<Character>
    {
        //Left empty because no special services needed for character. But can easily add new ones with this interface.
    }
}
