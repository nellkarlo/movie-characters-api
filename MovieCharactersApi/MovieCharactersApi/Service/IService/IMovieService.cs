﻿using MovieCharacters.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Service.IService
{
    public interface IMovieService : IGenericCrudService<Movie>
    {
        Task<IEnumerable<Character>> GetCharactersInMovie(int franchiseId);

    }
}
