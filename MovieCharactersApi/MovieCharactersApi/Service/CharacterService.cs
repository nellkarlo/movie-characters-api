﻿using Microsoft.EntityFrameworkCore;
using MovieCharacters.Model;
using MovieCharactersApi.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Service
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieDbContext _context;
        public CharacterService(MovieDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Adds a character to database
        /// </summary>
        /// <param name="character">The character to add to database</param>
        /// <returns>The character added</returns>
        public async Task<Character> AddAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }
        /// <summary>
        /// Deletes character from database
        /// </summary>
        /// <param name="id">Id of the character to delete</param>
        /// <returns></returns>
        public async Task DeleteAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Remove(character);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Checks if the provided id matches any existing character in database
        /// </summary>
        /// <param name="id">character id</param>
        /// <returns>True if exists, otherwise false</returns>
        public bool Exists(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }
        /// <summary>
        /// Gets all the character in the database
        /// </summary>
        /// <returns>An Enumerable list of characters</returns>
        public async Task<IEnumerable<Character>> GetAllAsync()
        {
            return await _context.Characters.ToListAsync();
        }
        /// <summary>
        /// Retrieves a specific character matching the id provided
        /// </summary>
        /// <param name="id">The id of the character</param>
        /// <returns></returns>
        public async Task<Character> GetSpecificAsync(int id)
        {
            return await _context.Characters.Where(c => c.Id == id).SingleOrDefaultAsync();
        }
        /// <summary>
        /// Uses entitystate to update the character in database. Then runs SaveChangesAsync
        /// </summary>
        /// <param name="character">The character to update</param>
        /// <returns></returns>
        public async Task UpdateAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
