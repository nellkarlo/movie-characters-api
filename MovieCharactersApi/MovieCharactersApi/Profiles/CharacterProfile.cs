﻿using AutoMapper;
using MovieCharacters.Model;
using MovieCharactersApi.Models.DTOs.CharacterDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>().ReverseMap();
            CreateMap<Character, CharacterEditDTO>().ReverseMap();
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();

        }
    }
}
