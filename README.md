<a href="https://dotnet.microsoft.com/download" alt=".NET target"><img alt=".NET target" src="https://img.shields.io/badge/dynamic/xml?color=%23512bd4&label=target&query=%2F%2FTargetFramework%5B1%5D&url=https%3A%2F%2Fraw.githubusercontent.com%2FZacharyPatten%2FTowel%2Fmain%2FSources%2FTowel%2FTowel.csproj&logo=.net" title="Go To .NET Download"></a>

### Instructions
1. Open Package Manager Console and run "update-database" 
2. Open your databases in SSMS or Server Explorer and look for "MovieDb"
3. Run project file and try some API-requests through SwaggerUI! :) 
